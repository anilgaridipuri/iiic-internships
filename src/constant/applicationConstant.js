export const ApplicationConstant = {
  HOME_PAGE_PATH: "/",
  LOGIN_URL_PATH: "/login",
  MYACCOUNT_URL: "/my-account",
  FORGOTPASSWORD_URL_PATH: "/forgotpassword",
};
